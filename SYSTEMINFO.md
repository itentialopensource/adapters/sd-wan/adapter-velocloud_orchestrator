# VMware VeloCloud Orchestrator

Vendor: VMware
Homepage: https://www.vmware.com/

Product: VMware VeloCloud Orchestrator
Product Page: https://sase.vmware.com/products/component-orchestrator

## Introduction
We classify VeloCloud Orchestrator into the SD-WAN/SASE domain as it provides a Software Defined Wide Area Network (SD-WAN) solution.

## Why Integrate
The VeloCloud Orchestrator adapter from Itential is used to integrate the Itential Automation Platform (IAP) with VeloCloud Orchestrator. With this adapter you have the ability to perform operations on items such as:

- Network
- Edge
- Configuration

## Additional Product Documentation
The [API documents for VeloCloud Orchestrator](https://developer.broadcom.com/xapis/vmware-sd-wan-orchestrator-api-v1/3.4.6/)